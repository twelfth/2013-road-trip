# Road Trip 2013

![Zion National Park](http://i.imgur.com/iYRJKYY.png)

# Thursday June 6

Convene in Salt Lake City.

![Family trip](http://i.imgur.com/uDHwcDz.jpg)  

# 2:03 PM: Jay arrives  

 Delta Airlines Flight 1275 

![](http://i.imgur.com/rGtBi2w.jpg)

# 6:32pm: Yen arrives 
 
United Airlines Flight 6470  

![](http://i.imgur.com/OomQXlk.jpg)

# 10:35pm: Claudia arrives 
 
JetBlue Flight 87

![](http://i.imgur.com/4OIFAxX.jpg)

# June 7: Zion National Park

![](http://i.imgur.com/iYRJKYY.png)

# June 7 and June 8: Ferber Campgrounds

(435) 772-3237
479 Zion Park Blvd.
Springdale,  UT  84767
$39.

# June 9

1. Either another night at 
2. # Zion/Bryce KOA

US 89, 5 miles north of Glendale
Box 189
Glendale, UT 84729

435-648-2490



# image-only

![](http://i.imgur.com/PwvoATc.jpg)

# image-only

![](http://i.imgur.com/YM6FlsH.jpg)

# image-only

![](http://i.imgur.com/IFaQ3cl.jpg)

# image-only

![](http://i.imgur.com/nPplMjS.jpg)

# image-only

![](http://i.imgur.com/iYRJKYY.png)

[More photos](http://pinterest.com/search/pins/?q=zion%20national%20park)

# June 10: Arches National Park

![](http://i.imgur.com/g08700v.jpg)

# image-only

![](http://i.imgur.com/NXRyK1L.jpg)

# image-only

![](http://i.imgur.com/F4mFFox.jpg)

# image-only

![](http://i.imgur.com/44ItnFg.jpg)

[More photos](http://pinterest.com/search/pins/?q=arches%20national%20park)

# June 11: Jay's birthday

![](http://i.imgur.com/NdX2K9y.jpg)

# Thursday June 13th

![Butte](http://i.imgur.com/DKK1Kug.jpg)

- Claudia flies out of Moab
- Yen and Jay arrive in Crested Butte

# image-only

![](http://i.imgur.com/RsUDXub.jpg)

# image-only

![](http://i.imgur.com/pvIrHlC.jpg)

# The Wedding!

Friday June 14 and Saturday June 15

Ilyse and Gian's wedding at Crusty Bottoms

![](http://i.imgur.com/RZOMr5u.jpg)

[Wedding information](http://ilyseandjohn.ourwedding.com/)

# Sunday June 16

Departures.

- Jay drives back to Salt Lake
- Yen flies out of Denver

![](http://i.imgur.com/9D5PS1p.jpg)

# Monday June 17

Departures, return car.

## 5:15pm
Jay flies out of Salt Lake City

![](http://i.imgur.com/2t09wYo.jpg)

# Itinerary

![](http://i.imgur.com/eSk4ZiF.png)

[Tentative Itinerary on Google Maps](http://goo.gl/maps/DA1iM)  
URL: [http://goo.gl/maps/DA1iM](http://goo.gl/maps/DA1iM)

# Expectations

1. conserve money
2. camp, rather than staying in motels
3. camp at campgrounds with electricity for Jay's respirator
4. listen to "playlists"
5. tell ghost stories
6. eat smores at least once
7. look at beauty/sheep

# Open questions

1. Is Eric joining us?
2. What kind of car are we renting?
3. Cooking, groceries? Hard-boiled eggs?
4. Can someone volunteer to pack a suitcase full of garbage and bring it along?

![](http://i.imgur.com/WcJlkbF.jpg)

# Resources

[Arches National Park photos](http://pinterest.com/search/pins/?q=arches%20national%20park)

[Zion National Park photos](http://pinterest.com/search/pins/?q=zion%20national%20park)

[Google Maps Itinerary](http://goo.gl/maps/DA1iM)

[John and Ilyse's wedding](http://ilyseandjohn.ourwedding.com/)

[Our photo album of the trip](http://imgur.com/a/1dkM9)


# Campgrounds

* Utah logistics

June 6 Salt Lake City

** June 7, 8, 9 

Zion National Park

## Watchman campground

recreation.gov

** June 10, 11, 12

Moab

** June 13


